#ifndef RANDOM_MATRIX
#define RANDOM_MATRIX

#include <iostream>
#include <vector>
#include "matrix.hpp"

int random_integer(int boundary_l, int boundary_u);

Matrix random_matrix(int m_height, int m_width, int boundary_l, int boundary_u, bool parallel_tag);

#endif