#include "matrix.hpp"
#include <chrono>
#include <iostream>
#include <vector>
#include <cstring>

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

int main(int argc, char* argv[]){
    bool parallel = false;

    for(int i = 1; i < argc; ++i){
        if(!strcmp(argv[i], "--help")){
            std::string usage = "Semestrálna prráca z predmetu PJC \nTento program slúži pre násobenie matíc.\nMatice je možné vynásobiť paralélne. Pre paralélny výpočet treba použiť --parallel.\nPo spustění je používateľ vyzvaný na vyplnenie údajov (s koľkými maticami chce počítať a počet riadkov a stĺpcov všetkých matíc)\nPo vyplnení údajov dôjde k násobeniu.\n";
            std::cout << usage;
            return 0;
        }else if(!strcmp(argv[i], "--parallel")){
            parallel = true;
        }else{
            std::cerr << "You can only use --help and --parallel!" << std::endl;
            return EXIT_FAILURE;
        }
    }

    int matrices_count, rows, cols;
    std::cout << "How many matrices do you want?: " << std::endl;
    std::cin >> matrices_count;

    if(std::cin.fail() || matrices_count < 2){
        std::cout << "You can only multiply 2 or more matrices!" << std::endl;
        return EXIT_FAILURE;
    }else{
        std::vector<Matrix> matrices(matrices_count);

        for (int i = 0; i < matrices_count; ++i) {
            std::cout << "Number of rows: " << std::endl;
            std::cin >> rows;

            if(std::cin.fail()){
                std::cout << "Matrix must have 1 or more rows (integer)!" << std::endl;
                return EXIT_FAILURE;
            }

            std::cout << "Number of columns: " << std::endl;
            std::cin >> cols;

            if(std::cin.fail()){
                std::cout << "Matrix must have 1 or more columns (integer)!" << std::endl;
                return EXIT_FAILURE;
            }

            std::vector<std::vector<int>> mat(rows);

            for (int j = 0; j < rows; ++j) {
                std::vector<int> row(cols);

                for (int k = 0; k < cols; ++k) {
                    std::cin >> row[k];

                    if(std::cin.fail()){
                        std::cout << "You can only use integers in the matrix!" << std::endl;
                        return EXIT_FAILURE;
                    }
                }

                mat[j] = row;
            }

            matrices[i] = Matrix(mat, parallel);
        }

        try {
            Matrix res = matrices[0] * matrices[1];

            for (int i = 2; i < matrices_count; ++i) {
                res = res * matrices[i];
            }

            std::cout << res;
        }catch(const invalid_dim& ex){
            std::cout << ex.what() << std::endl;
            return EXIT_FAILURE;
        }
    }
}
