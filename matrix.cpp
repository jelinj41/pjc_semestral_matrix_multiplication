#include "matrix.hpp"

#include <iostream>
#include <vector>
#include <thread>
#include <future>

Matrix::Matrix(Matrix &&rhs) {
    this->matrix = rhs.matrix;
    this->m_height = rhs.m_height;
    this->m_width = rhs.m_width;
    this->parallel = rhs.parallel;
}

Matrix::Matrix(const Matrix &rhs) {
    this->matrix = rhs.matrix;
    this->m_height = rhs.m_height;
    this->m_width = rhs.m_width;
    this->parallel = rhs.parallel;
}

Matrix& Matrix::operator=(const Matrix &rhs) {
    this->matrix = rhs.matrix;
    this->m_height = rhs.m_height;
    this->m_width = rhs.m_width;
    this->parallel = rhs.parallel;
    return *this;
}

Matrix& Matrix::operator=(Matrix &&rhs) {
    this->matrix = rhs.matrix;
    this->m_height = rhs.m_height;
    this->m_width = rhs.m_width;
    this->parallel = rhs.parallel;
    return *this;
}

Matrix::Matrix(std::vector<std::vector<int>> &data, bool parallel_tag){
    this->matrix = data;
    this->m_height = data.size();
    this->m_width = data[0].size();
    this->parallel = parallel_tag;
}


Matrix transpose_matrix(Matrix &rhs) {
    std::vector<std::vector<int>> trans(rhs.m_width);

    for (unsigned int i = 0; i < rhs.m_height; ++i) {
        for (unsigned int j = 0; j < rhs.m_width; ++j) {
            trans[j].push_back(rhs.matrix[i][j]);
        }
    }

    return Matrix(trans, rhs.parallel);
}

Matrix multiplication_basic(Matrix const &lhs, Matrix const &rhs){
    std::vector<std::vector<int>> new_m(lhs.m_height);
    Matrix trans_rhs = transpose_matrix(const_cast<Matrix &>(rhs));

    for (unsigned int i = 0; i < lhs.m_height; ++i) {
        std::vector<int> m_row(trans_rhs.m_height, 0);

        for (unsigned int j = 0; j < trans_rhs.m_height; ++j) {
            m_row[j] = std::inner_product(lhs.matrix[i].begin(), lhs.matrix[i].end(), trans_rhs.matrix[j].begin(), 0);
        }

        new_m[i] = m_row;
    }

    return Matrix(new_m, false);
}


void matrix_part(Matrix const &lhs, Matrix const &rhs, std::vector<std::vector<int>> &tmp, int thread, int threshold){
    int num_threads = std::thread::hardware_concurrency();
    int size = thread == num_threads - 1 ? lhs.m_height : threshold * (thread + 1);

    for (int i = thread*threshold; i < size; ++i) {
        std::vector<int> m_row(rhs.m_height);

        for (unsigned int j = 0; j < rhs.m_height; ++j) {
            m_row[j] = std::inner_product(lhs.matrix[i].begin(), lhs.matrix[i].end(), rhs.matrix[j].begin(), 0);
        }

        tmp[i] = m_row;
    }
}

Matrix multiplication_basic_prl(Matrix const &lhs, Matrix const &rhs){
    std::vector<std::vector<int>> new_m(lhs.m_height);
    Matrix trans_rhs = transpose_matrix(const_cast<Matrix &>(rhs));
    int num_threads = std::thread::hardware_concurrency();
    int part_size = lhs.m_height / num_threads;
    std::vector<std::thread> threads;

    for (int i = 0; i < num_threads; ++i) {
        threads.emplace_back(matrix_part,
                             std::ref(lhs),
                             std::ref(trans_rhs),
                             std::ref(new_m),
                             i,
                             part_size);
    }

    for (auto& t : threads) {
        t.join();
    }

    return Matrix(new_m, true);
}


