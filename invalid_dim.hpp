#pragma once
#include <exception>

class invalid_dim : public std::exception{
public:
    const char* what() const throw(){
        return "Invalid dimension of the matrices.";
    }
};