#pragma once
#include <iostream>
#include <vector>
#include <algorithm>
#include<bits/stdc++.h>
#include "invalid_dim.hpp"

class Matrix{
public:
    Matrix() = default;
    Matrix(const Matrix& rhs);
    Matrix& operator=(const Matrix& rhs);
    Matrix(Matrix&& rhs);
    Matrix& operator=(Matrix&& rhs);
    Matrix(std::vector<std::vector<int>> &data, bool parallel_tag);
    ~Matrix()  = default;

    friend std::ostream& operator<<(std::ostream &os, Matrix const &m);
    friend Matrix multiplication_basic(Matrix const &lhs, Matrix const &rhs);
    friend Matrix multiplication_basic_prl(Matrix const &lhs, Matrix const &rhs);
    friend void matrix_part(Matrix const &lhs, Matrix const &rhs, std::vector<std::vector<int>> &tmp, int thread, int threshold);
    friend Matrix transpose_matrix(Matrix &rhs);

    Matrix operator*(Matrix const &rhs) const{
        if(this->m_width != rhs.m_height){
            std::string error = "Wrong dimensions for multiplication.\n Can not multiply matrices:\n" + std::to_string(this->m_height) + "x" + std::to_string(this->m_width) + " with " + std::to_string(rhs.m_height) + "x" + std::to_string(rhs.m_width) + "\n";
            throw invalid_dim();
        }

        if(this->parallel){
            return multiplication_basic_prl(*this, rhs);
        }else{
            return multiplication_basic(*this, rhs);
        }
    }

private:
    unsigned int m_height;
    unsigned int m_width;
    std::vector<std::vector<int>> matrix;
    bool parallel;
};

inline std::ostream& operator<<(std::ostream &os, Matrix const &m){
    for (unsigned int i = 0; i < m.matrix.size(); ++i) {
        for (unsigned int j = 0; j < m.matrix[i].size(); ++j) {
            os << m.matrix[i][j] << " ";
        }

        os << std::endl;
    }

    return os;
}