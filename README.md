# Násobenie matíc

## Popis
Táto semestrálna práca sa zaoberá násobením matíc v jazyku C++. Násobenie matíc je jednou z najzaujímavejších operácií v lineárnej algebre.

## Implementácia
Napriek tomu, že sa dá táto operácia veľmi jednoducho naimplementovať za pomoci 3 for loopov, vybral som si spôsob, pri ktorom sa najprv transponuje pravá matica, čím sa ušetrí jeden for loop. Táto implementácia je výhodná aj z dôvodu, že sa dá jednoducho paralelizovať.

## Návod na používanie
Po spustení programu je používateľ vyzvaný aby zadal s koľkými maticami chce previesť operáciu. Je potrebné zadať číslo väčšie alebo rovné 2, aby mohol program správne fungovať. Následne je používateľ vyzvaný aby zadal počet riadkov a stĺpcov všetkých matíc a ich obsah, ktorý môže zapísať buď do jedného riadku, postupne do riadkov, alebo graficky (nutné podotknúť, že musíme pracovať iba s integer hodnotou). Následne sa 2 matice vynásobia podľa pravidiel násobenia a graficky sa vypíše výsledná matica.

## Kompilácia
Program sa dá spustiť v príkazovom riadku po otvorení priečinku *cmake-build-debug* a zadaní:
```bash
main.exe 
```
Priečinok tiež obsahuje testovacie vstupné dáta, ktoré môžeme vložiť do programu zadaním:
```bash
main.exe <example.txt
```
Program obsahuje prepínač na paralélne vynásobenie matíc, ktoré spustíme zadaním:
Do něj napište:
```bash
main.exe --parallel
```
Program obsahuje pomôcku na používanie programu, ktorú spustíme zadaním:
```bash
main.exe --help
```

## Meranie
Meranie probehlo na 12 jadrovom i7-8750H.

Na meranie som použil matice o rozmere 1000x1000 (súbor sa mi bohužiaľ zmazal)

Jedno vlákno:
```bash
270.28ms
```

Viac vlákien:
```bash
83.41ms
```
