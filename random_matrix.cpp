#include <vector>
#include <random>
#include "random_matrix.hpp"

int random_integer(int boundary_l, int boundary_u) {
    static std::mt19937 mt{ std::random_device{}() };
    static std::uniform_real_distribution<> dist(boundary_l, boundary_u);
    return dist(mt);
}

Matrix random_matrix(int m_height, int m_width, int boundary_l, int boundary_u, bool parallel_tag){
    std::vector<std::vector<int>> res(m_height);

    for (int i = 0; i < m_height; ++i) {
        std::vector<int> m_row(m_width);

        for (int j = 0; j < m_width; ++j) {
            m_row[j] = random_integer(boundary_l, boundary_u);
        }
        res[i] = m_row;
    }

    return Matrix(res, parallel_tag);
}
